// Copyright 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/Equanox/gotron"

	"gitlab.com/redfield/redctl/api"
)

// gui represents the state of the redctl-gui Electron application
type gui struct {
	window *gotron.BrowserWindow
	status chan bool
	rc     *api.Client
	domain *api.Domain
	ctx    context.Context
	cancel context.CancelFunc
}

// guiMesssage represents an event message with a data payload which can be sent to the JavaScript application
type guiMessage struct {
	*gotron.Event
	Data interface{} `json:"data"`
}

// sendMessage sends a message of type mType to the JavaScript application
func (ui *gui) sendMessage(mType string, data interface{}) error {
	msg := &guiMessage{
		Event: &gotron.Event{Event: mType},
		Data:  data,
	}

	return ui.window.Send(msg)
}

// logErrorWithNotify is a convenience function that will log and notify the
// JS application of e.
func (ui *gui) logErrorWithNotify(e error) {
	if err := ui.sendMessage("error", e.Error()); err != nil {
		log.Printf("Failed to notify UI of error (message=%v): %v", e, err)
	}

	log.Printf("Error: %v", e)
}

// spawn starts a new UI application window
func spawn(title string, appPath string, width int, height int, icon string) (*gui, error) {
	window, err := gotron.New(appPath)
	if err != nil {
		return nil, err
	}

	window.WindowOptions.Icon = icon

	window.WindowOptions.Title = title

	window.WindowOptions.Width = width
	window.WindowOptions.Height = height

	window.WindowOptions.Resizable = false

	ctx, cancel := context.WithCancel(context.Background())

	ui := gui{
		window: window,
		ctx:    ctx,
		cancel: cancel,
	}

	if ui.status, err = ui.window.Start(); err != nil {
		return nil, err
	}

	ui.waitForJSReady()

	return &ui, nil
}

// Close closes all open client connections used by the UI
func (ui *gui) Close() error {
	var err error

	if ui.rc != nil {
		ui.rc.Close()
	}

	if err != nil {
		return errors.New("one or more client connections failed to close")
	}

	return nil
}

// Start opens the UI, registers event handlers, sends UI state, and waits for completion
func Start(addr string, icon string) error {
	if addr == "" {
		return errors.New("redctl address empty")
	}

	// Instantiate the UI window
	ui, err := spawn("VM Management", "app", 800, 600, icon)
	if err != nil {
		return err
	}

	defer func() {
		if err := ui.Close(); err != nil {
			log.Printf("failed to close client connections: %v", err)
		}
	}()

	rc := api.NewClient().WithTimeout((10 * time.Second).String()).WithServerURL(addr)

	if err = rc.Dial(); err != nil {
		log.Printf("unable to establish connection to redctl: %v", err)
		return err
	}

	ui.rc = rc

	// Handle events
	ui.registerDoneEventHandler()

	ui.registerVMCreateEventHandler()

	ui.registerImageCreateEventHandler()

	ui.registerVMUpdateEventHandler()

	// Send data
	if err = ui.sendBackendsEvent(); err != nil {
		log.Println(err)
		return err
	}

	if err = ui.sendImagesEvent(); err != nil {
		log.Println(err)
		return err
	}

	if err = ui.sendGraphicsEvent(); err != nil {
		log.Println(err)
		return err
	}

	// wait for user to submit or close the UI window, or an error occurs
	// triggered by 'ui.window.Close()'
	<-ui.status

	return nil
}
