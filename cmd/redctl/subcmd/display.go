// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// displayCmd represents the display command
var (
	displayCmd = &cobra.Command{
		Use:   "display",
		Short: "Manage display settings",
		Long:  `Set resolution, etc.`,
	}
)

var (
	displayForceResolutionCmd = &cobra.Command{
		Use:   "force-resolution",
		Short: "Override resolution for primary display",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("display force-resolution called")
			mode := viper.GetString("mode")

			client := initializeRedctlClient()
			defer client.Close()

			if err := client.DisplayForceResolution(mode); err != nil {
				log.Fatalf("Failed to set resolution mode: %v", err)
			}

			if mode == "" {
				fmt.Println("mode cleared")
			} else {
				fmt.Println("mode set to:", mode)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(displayCmd)

	displayCmd.AddCommand(displayForceResolutionCmd)
	displayForceResolutionCmd.Flags().String("mode", "", "resolution to display (e.g. 1920x1080")
}
