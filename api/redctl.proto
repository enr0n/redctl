syntax = "proto3";

package api;
import "google/protobuf/timestamp.proto";

service redctl {
    rpc Ping (PingRequest) returns (PingReply) {}

    rpc SubscribeEvents(SubscribeEventsRequest) returns (stream SubscribeEventsReply) {} 
    
    rpc GraphicImport(GraphicImportRequest) returns (GraphicImportReply) {}
    rpc GraphicRemove(GraphicRemoveRequest) returns (GraphicRemoveReply) {}
    rpc GraphicFind(GraphicFindRequest) returns (GraphicFindReply) {}
    rpc GraphicFindAll(GraphicFindAllRequest) returns (GraphicFindAllReply) {}

    rpc DomainCreate (DomainCreateRequest) returns (DomainCreateReply) {}
    rpc DomainFind (DomainFindRequest) returns (DomainFindReply) {}
    rpc DomainFindAll (DomainFindAllRequest) returns (DomainFindAllReply) {}
    rpc DomainUpdate (DomainUpdateRequest) returns (DomainUpdateReply) {}
    rpc DomainRemove (DomainRemoveRequest) returns (DomainRemoveReply) {}
    rpc DomainStart (DomainStartRequest) returns (DomainStartReply) {}
    rpc DomainStop (DomainStopRequest) returns (DomainStopReply) {}
    rpc Shutdown (ShutdownRequest) returns (ShutdownReply) {}
    rpc DomainRestart (DomainRestartRequest) returns (DomainRestartReply) {}

    rpc DomainHotplugNetworkAttach (DomainHotplugNetworkAttachRequest) returns (DomainHotplugNetworkAttachReply) {}
    rpc DomainHotplugNetworkDetach (DomainHotplugNetworkDetachRequest) returns (DomainHotplugNetworkDetachReply) {}

    rpc DomainHotplugPciAttach (DomainHotplugPciAttachRequest) returns (DomainHotplugPciAttachReply) {}
    rpc DomainHotplugPciDetach (DomainHotplugPciDetachRequest) returns (DomainHotplugPciDetachReply) {}

    rpc ImageCreate (ImageCreateRequest) returns (ImageCreateReply) {}
    rpc ImageCreateBacked (ImageCreateBackedRequest) returns (ImageCreateBackedReply) {}
    rpc ImageCopy (ImageCopyRequest) returns (ImageCopyReply) {}
    rpc ImageFind (ImageFindRequest) returns (ImageFindReply) {}
    rpc ImageRemove (ImageRemoveRequest) returns (ImageRemoveReply) {}

    rpc UpgradeControlDomain (UpgradeControlDomainRequest) returns (UpgradeControlDomainReply) {}

    rpc DisplayForceResolution(DisplayForceResolutionRequest) returns (DisplayForceResolutionReply) {}

    rpc LuksAddKey (LuksAddKeyRequest) returns (LuksAddKeyReply) {}
    rpc LuksRotateKey (LuksRotateKeyRequest) returns (LuksRotateKeyReply) {}
    rpc LuksRemoveKey (LuksRemoveKeyRequest) returns (LuksRemoveKeyReply) {}
}

message PingRequest {
    string msg = 1;
}

message PingReply {
    string msg = 1;
}

message Event {
    enum Type {
        DOMAIN_CREATED = 0;
        DOMAIN_REMOVED = 1;
        DOMAIN_STARTED = 2;
        DOMAIN_STOPPED = 3;
        NETWORK_ATTACH = 4;
        NETWORK_DETACH = 5;
    }
    Type type = 1;
    oneof data {
        Domain domain = 2;
    }
    oneof device {
        DomainNetwork network = 3;
    }
}

message SubscribeEventsRequest {
}

message SubscribeEventsReply {
    Event event = 1;
}

enum GraphicType {
    GRAPHIC_UNSPECIFIED = 0;
    GRAPHIC_DESKTOP_ICON = 1;
    GRAPHIC_BACKGROUND = 2;
}

message Graphic {
    string name = 1;
    bytes data = 2;
    GraphicType type = 3;
    // Path is ONLY used to give the absolute
    // path of the graphic in Find requests.
    string path = 4;
}

message GraphicImportRequest {
    Graphic graphic = 1;
}

message GraphicImportReply {
}

message GraphicRemoveRequest {
    Graphic graphic = 1;
}

message GraphicRemoveReply {
}

message GraphicFindRequest {
    Graphic graphic = 1;
}

message GraphicFindReply {
    Graphic graphic = 1;
}

message GraphicFindAllRequest {
    GraphicType type = 1;
}

message GraphicFindAllReply {
    repeated Graphic graphics = 1;
}

enum ImageFormat {
    UNSPECIFIED = 0;
    RAW = 1;
    QCOW2 = 2;
    ISO = 3;
}

message Image {
    string name = 1;
    int64 size = 2;
    ImageFormat format = 3;
    string path = 4;

    google.protobuf.Timestamp last_updated = 7;
}

enum DomainType {
    DOMAIN_TYPE_UNSPECIFIED = 0;
    DOMAIN_TYPE_PV = 1;
    DOMAIN_TYPE_PVH = 2;
    DOMAIN_TYPE_HVM = 3;
}

enum DomainEvent {
    DOMAIN_EVENT_UNSPECIFIED = 0;
    DOMAIN_EVENT_DESTROY = 1;
    DOMAIN_EVENT_RESTART = 2;
    DOMAIN_EVENT_RENAME_RESTART = 3;
    DOMAIN_EVENT_PRESERVE = 4;
    DOMAIN_EVENT_COREDUMP_DESTROY = 5;
    DOMAIN_EVENT_COREDUMP_RESTART = 6;
    DOMAIN_EVENT_SOFT_RESET = 7;
}

enum DomainPowerState {
    DOMAIN_POWER_UNSPECIFIED = 0;
    DOMAIN_POWER_RUNNING = 1;
    DOMAIN_POWER_BLOCKED = 2;
    DOMAIN_POWER_PAUSED = 3;
    DOMAIN_POWER_SHUTDOWN = 4;
    DOMAIN_POWER_CRASHED = 5;
    DOMAIN_POWER_DYING = 6;
    DOMAIN_POWER_STOPPED = 7;
}

message DomainDisk {
    string target = 1;
    string format = 2;
    string vdev = 3;
    string access = 4;
    string devtype = 5;
    string backend = 6;
    string backendtype = 7;
    string script = 8;
    string discard = 9;
    Image image = 10;
}

message DomainNetwork {
    string mac = 1;
    string bridge = 2;
    string gatewaydev = 3;
    string type = 4;
    string model = 5;
    string vifname = 6;
    string script = 7;
    string ip = 8;
    string backend = 9;
    string rate = 10;
    string devid = 11;
}

message DomainVirtualNuma {
    string pnode = 1;
    string size = 2;
    string vcpus = 3;
    string vdistances = 4;
}

message DomainPciDevice {
    string address = 1;
    string vslot = 2;
    string permissive = 3;
    string msitranslate = 4;
    string seize = 5;
    string power_mgmt = 6;
    string rdm_policy = 7;
}

message DomainUsbController {
    string type = 1;
    string version = 2;
    string ports = 3;
}

message DomainConfiguration {
    string uuid = 1;
    string name = 2;
    //string description = 3;
    string type = 4;
    //string pool = 5;
    string vcpus = 6;
    //string maxvcpus = 7;
    //string cpus = 8;
    //string cpus_soft = 9;
    //string cpu_weight = 10;
    //string cap = 11;
    string memory = 12;
    //string maxmem = 13;
    //repeated DomainVirtualNuma numas = 14;
    DomainEvent on_poweroff = 15;
    DomainEvent on_reboot = 16;
    DomainEvent on_watchdog = 17;
    DomainEvent on_crash = 18;
    DomainEvent on_soft_reset = 19;
    string kernel = 20;
    string kernel_extract = 21;
    string ramdisk = 22;
    string ramdisk_extract = 23;
    string cmdline = 24;
    //string seclabel = 23;
    //string init_seclabel = 24;
    //string max_grant_frames = 25;
    //string max_maptrack_frames = 26;
    string driver_domain = 27;
    //string device_tree = 28;
    repeated DomainDisk disks = 29;
    repeated DomainNetwork networks = 30;
    //repeated DomainVtpm vtpms = 31;
    //repeated DomainNinePfs pfs = 32;
    //repeated DomainPvCalls pvcalls = 33;
    //repeated DomainVfb vfbs = 34;
    //repeated DomainChannel channels = 35;
    //string rdm = 36;
    repeated DomainUsbController usbctrls = 37;
    //repeated DomainUsbDevice usbdevs = 38;
    repeated DomainPciDevice pcidevs = 39;
    //string gfxpassthru = 40;
    //string rdm_mem_boundary = 41;
    //repeated DomainDtDevice dtdevs = 42;
    //repeated DomainIoPorts ioports = 43;
    //repeated DomainIoMem iomems = 44;
    //repeated int irqs = 45;
    //string max_event_channels = 46;
    //repeated DomainVirtualDisplay vdisplays = 47;
    //string dm_restrict = 48;
    //repeated DomainSoundDevice vsnds = 49;
    //repeated DomainVkbdDevice vkbs = 50;

    // HVM Guest Specific Options
    string boot = 51;
    //string hdtype = 52;

    // Processor and Platform Features
    //string bios = 53;
    //string bios_path_override = 54;
    //string acpi = 55;
    //string acpi_s3 = 56;
    //string acpi_s4 = 57;
    //string acpi_laptop_slate = 58;
    string usb = 59;
    string usbdevice = 60;
    string vga = 61;
    string sdl = 62;
    string opengl = 63;
    string vnc = 64;
    string soundhw = 65;
    repeated string device_model_args = 66;
}

message Domain {
    string uuid = 1;
    DomainConfiguration config = 2;
    DomainPowerState power_state = 3;
    bool start_on_boot = 4;
    string desktop_icon = 5;

    google.protobuf.Timestamp last_updated = 6;
}

// IMAGE

message ImageCreateRequest {
    Image image = 1;
}

message ImageCreateReply {
    Image image = 1;
}

message ImageCreateBackedRequest {
    Image image = 1;
    Image backingImage = 2;
}

message ImageCreateBackedReply {
    Image image = 1;
}

message ImageCopyRequest {
    string source = 1;
    string destination = 2;
}

message ImageCopyReply {
    Image image = 1;
}

message ImageFindRequest {
    string name = 1;
}

message ImageFindReply {
    repeated Image images = 1;
}

message ImageUpdateRequest {
    Image image = 1;
}

message ImageUpdateReply {
    Image image = 1;
}

message ImageRemoveRequest {
    string name = 1;
}

message ImageRemoveReply {
}

// DOMAIN

message DomainCreateRequest {
    Domain domain = 1;
}

message DomainCreateReply {
    Domain domain = 1;
}

message DomainFindRequest {
    string uuid = 1;
}

message DomainFindReply {
    Domain domain = 1;
}

message DomainFindAllRequest {
}

message DomainFindAllReply {
    repeated Domain domains = 1;
}

message DomainUpdateRequest {
    Domain domain = 1;
}

message DomainUpdateReply {
    Domain domain = 1;
}

message DomainRemoveRequest {
    string uuid = 1;
}

message DomainRemoveReply {
}

message DomainStartRequest {
    string uuid = 1;
}

message DomainStartReply {
}

message DomainStopRequest {
    string uuid = 1;
}

message DomainStopReply {
}

message ShutdownRequest {
    uint32 timeout = 1;
}

message ShutdownReply {
}

message DomainRestartRequest {
    string uuid = 1;
}

message DomainRestartReply {
}

message DomainHotplugNetworkAttachRequest {
    Domain domain = 1;
    DomainNetwork network = 2;
}

message DomainHotplugNetworkAttachReply {
}

message DomainHotplugNetworkDetachRequest {
    Domain domain = 1;
    DomainNetwork network = 2;
}

message DomainHotplugNetworkDetachReply {
}

message DomainHotplugPciAttachRequest {
    string uuid = 1;
    DomainPciDevice pcidev = 2;
}

message DomainHotplugPciAttachReply {
}

message DomainHotplugPciDetachRequest {
    string uuid = 1;
    DomainPciDevice pcidev = 2;
}

message DomainHotplugPciDetachReply {
}

message UpgradeControlDomainRequest {
    string name = 1;
}

message UpgradeControlDomainReply {
}

message DisplayForceResolutionRequest {
    string mode = 1;
}

message DisplayForceResolutionReply {
}

message LuksAddKeyRequest {
    bytes existing_key = 1;
    bytes new_key = 2;
}

message LuksAddKeyReply {
}

message LuksRotateKeyRequest {
    bytes existing_key = 1;
    bytes new_key = 2;
}

message LuksRotateKeyReply {
}

message LuksRemoveKeyRequest {
    bytes remove_key = 1;
}

message LuksRemoveKeyReply {
}
