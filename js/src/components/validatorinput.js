// Copyright 2019 Assured Information Security, Inc.

import React, {Component} from 'react';

import LabelInput from './labelinput';

const style = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '95%',
    backgroundColor: 'inherit'
};

const Indicator = (props) => {
    let component = (null);

    switch (props.display) {
    case 'valid':
        component = (<div className={'checkmark'} style={{ color: 'green' }}>L</div>);
        break;
    case 'invalid':
        component = (<h5 style={{ color: 'red' }}>X</h5>);
        break;
    case 'loading':
        component = (<p>...</p>);
        break;
    }

    return component;
};

export default class ValidatorInput extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);

        this.state = {};
    }

    getValue() {
        return this.state.value !== undefined ? this.state.value : this.props.value;
    }

    onChange(value) {
        let lastKeyEvent = Math.random();

        this.setState((state) => ({ ...state, lastKeyEvent, value }));

        setTimeout(
            () => {
                if (this.state.lastKeyEvent === lastKeyEvent && typeof this.props.onChange == 'function') {
                    this.props.onChange(value);
                    this.setState((state) => ({ ...state, value: undefined }));
                }
            },
            750
        );
    }

    render() {
        return (
            <div style={style}>
                <LabelInput {...this.props} onChange={this.onChange} value={this.getValue()} />
                <Indicator display={this.props.display} />
            </div>
        );
    }
}
