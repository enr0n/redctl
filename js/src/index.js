// Copyright 2019 Assured Information Security, Inc.

import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/app';

ReactDOM.render(<App />, document.getElementById('app'));
