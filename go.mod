module gitlab.com/redfield/redctl

require (
	github.com/Equanox/gotron v0.2.23
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/coreos/go-etcd v2.0.0+incompatible // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gofrs/flock v0.7.1
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/jaypipes/ghw v0.0.0-20190131082148-032dfb1f8cb2 // indirect
	github.com/jaypipes/pcidb v0.0.0-20190216134740-adf5a9192458 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20180621225840-336beac0a992
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.7.1
	github.com/ugorji/go/codec v0.0.0-20181204163529-d75b2dcb6bc8 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	google.golang.org/grpc v1.21.1
	howett.net/plist v0.0.0-20181124034731-591f970eefbb // indirect
)

go 1.11
