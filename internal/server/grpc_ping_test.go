// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"testing"

	"gitlab.com/redfield/redctl/api"
)

func TestPing(t *testing.T) {
	s := server{}

	var req api.PingRequest
	msg := "testing 1... 2... 3..."
	req.Msg = msg
	reply, err := s.Ping(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to ping: %v\n", err)
	}

	if reply.Msg != msg {
		t.Fatalf("Ping reply != request: %v\n", err)
	}
}
