// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/google/uuid"
	scribble "github.com/nanobox-io/golang-scribble"
	"gitlab.com/redfield/redctl/api"
)

func newTestScribbleRepo() (DomainRepo, error) {
	path, err := ioutil.TempDir(os.TempDir(), "scribble-test")
	if err != nil {
		return nil, err
	}

	dcPath := filepath.Join(path, "domains")

	err = os.MkdirAll(dcPath, 0755)
	if err != nil {
		return nil, err
	}

	db, err := scribble.New(path, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create database for redctl: %v", err)
	}

	return NewScribbleDomainRepo(db), err

}

func TestCreateDuplicate(t *testing.T) {
	repo, err := newTestScribbleRepo()
	if err != nil {
		t.Fatalf("failed to create test repo")
	}

	d := api.NewUserDomain("test1")
	d.Uuid = uuid.New().String()

	if err := repo.Create(d); err != nil {
		t.Fatalf("failed to create domain: %v", err)
	}

	if err := repo.Create(d); err == nil {
		t.Fatal("expected error when creating duplicate domain")
	}
}
